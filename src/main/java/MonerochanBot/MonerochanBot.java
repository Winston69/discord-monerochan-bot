package MonerochanBot;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;

import javax.security.auth.login.LoginException;

public class MonerochanBot {

    public static JDA jda;

    public static final String MONEROCHAN_BOT_NAME = "Monerochan";
    public static final String MONEROCHAN_BOT_API_KEY = "";
    public static final String MONEROCHAN_CURRENT_GAME = "Boating Simulator";

    public static void main (String[] args) throws LoginException {
        jda = JDABuilder.createDefault(MONEROCHAN_BOT_API_KEY)
                .setActivity(Activity.playing(MONEROCHAN_CURRENT_GAME))
                .setStatus(OnlineStatus.ONLINE)
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .addEventListeners(new Commands(), new MemberJoinResponse(), new ServerJoinResponse())
                .build();
    }
}
