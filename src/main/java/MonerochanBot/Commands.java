package MonerochanBot;

import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.Arrays;

import static MonerochanBot.MonerochanBot.MONEROCHAN_BOT_NAME;

public class Commands extends ListenerAdapter {

    private static final String COMMAND_PREFIX = "/";

    private static final String ABOUT_COMMAND = "/about";
    private static final String BIBLE_COMMAND = "/bible";
    private static final String ELI5_COMMAND = "/eli5";
    private static final String GLOSSARY_COMMAND = "/glossary";

    private static final String GET_XMR_COMMAND = "/getxmr";
    private static final String GET_XMR_LM_COMMAND = "/lm";
    private static final String MINING_XMR_COMMAND = "/mining";

    private static final String HELP_COMMAND = "/help";
    private static final String COMMANDS_COMMAND = "/commands";

    private static final String HI_COMMAND = "/hi";
    private static final String ART_COMMAND = "/art";
    private static final String RICH_LIST_COMMAND = "/richlist";

    private static final String WALLET_TYPES_COMMANDS = "/wallettypes";
    private static final String GUI_WALLET_COMMAND = "/gui";
    private static final String CLI_WALLET_COMMAND = "/cli";
    private static final String CAKE_WALLET_COMMAND = "/cake";
    private static final String FEATHER_WALLET_COMMAND = "/feather";
    private static final String MONERUJO_COMMAND = "/monerujo";
    private static final String MYMONERO_COMMAND = "/mymonero";
    private static final String PAPER_WALLET_GUIDE_COMMAND = "/paper";
    private static final String HARDWARE_WALLETS_COMMAND = "/hardware";
    private static final String ALL_WALLETS_COMMAND = "/allwallets";

    private static final String XMR_PRICE_COMMAND = "/price";
    private static final String XMR_TO_USD_COMMAND = "/xmrusd";
    private static final String USD_TO_XMR_COMMAND = "/usdxmr";
    private static final String XMR_TO_BTC_COMMAND = "/xmrbtc";
    private static final String BTC_TO_XMR_COMMAND = "/btcxmr";

    private static final String DONATE_GENERAL_FUND_COMMAND = "/donate";
    private static final String DONATE_ART_COMMAND = "/donateart";
    private static final String DONATE_DEV_COMMAND = "/donatedev";

    public void onMessageReceived(MessageReceivedEvent msgReceivedEvent){
        String[] msgArray = msgReceivedEvent.getMessage().getContentRaw().split("\\s+");
        String authorName = msgReceivedEvent.getAuthor().getName();
        Responses responses = new Responses();

        MessageChannel channel = getMessageChannel(msgReceivedEvent);
        if (msgArray[0].startsWith(COMMAND_PREFIX)) {
            msgReceivedEvent.getChannel().sendTyping().queue();
            String command = msgArray[0];
            boolean secondWordPresent = msgArray.length >= 2;

            switch (command) {
                case HI_COMMAND:
                    channel.sendMessage(responses.hiReply(authorName)).queue();
                    break;
                case ABOUT_COMMAND:
                    channel.sendMessageEmbeds(responses.aboutReply()).queue();
                    break;
                case ELI5_COMMAND:
                    channel.sendMessageEmbeds(responses.eliFiveReply()).queue();
                    break;
                case GLOSSARY_COMMAND:
                    channel.sendMessageEmbeds(responses.glossaryReply()).queue();
                    break;
                case GET_XMR_COMMAND:
                    channel.sendMessageEmbeds(responses.getXmrGuideReply()).queue();
                    break;
                case GET_XMR_LM_COMMAND:
                    channel.sendMessageEmbeds(responses.localMoneroGuide()).queue();
                    break;
                case MINING_XMR_COMMAND:
                    channel.sendMessageEmbeds(responses.getMiningGuideReply()).queue();
                    break;
                case BIBLE_COMMAND:
                    channel.sendMessageEmbeds(responses.masteringMoneroReply()).queue();
                    break;
                case HELP_COMMAND:
                    channel.sendMessage(responses.helpReply()).queue();
                    break;
                case COMMANDS_COMMAND:
                    channel.sendMessageEmbeds(responses.commandsReply()).queue();
                    break;
                case ART_COMMAND:
                    channel.sendMessageEmbeds(responses.artReply()).queue();
                    break;
                case RICH_LIST_COMMAND:
                    channel.sendMessageEmbeds(responses.richListReply()).queue();
                    break;
                case WALLET_TYPES_COMMANDS:
                    channel.sendMessageEmbeds(responses.walletTypesReply()).queue();
                    break;
                case GUI_WALLET_COMMAND:
                    channel.sendMessageEmbeds(responses.guiWalletReply()).queue();
                    break;
                case CLI_WALLET_COMMAND:
                    channel.sendMessageEmbeds(responses.cliWalletReply()).queue();
                    break;
                case CAKE_WALLET_COMMAND:
                    channel.sendMessageEmbeds(responses.cakeWalletReply()).queue();
                    break;
                case FEATHER_WALLET_COMMAND:
                    channel.sendMessageEmbeds(responses.featherWalletReply()).queue();
                    break;
                case MONERUJO_COMMAND:
                    channel.sendMessageEmbeds(responses.monerujoReply()).queue();
                    break;
                case MYMONERO_COMMAND:
                    channel.sendMessageEmbeds(responses.myMoneroWalletReply()).queue();
                    break;
                case PAPER_WALLET_GUIDE_COMMAND:
                    channel.sendMessageEmbeds(responses.sendPaperWalletGuideReply()).queue();
                    break;
                case HARDWARE_WALLETS_COMMAND:
                    channel.sendMessageEmbeds(responses.sendHardwareWalletsReply()).queue();
                    break;
                case ALL_WALLETS_COMMAND:
                    if (!checkIfPrivateMsg(msgReceivedEvent)) {
                        channel.sendMessage(responses.mustBePrivateMessageReply()).queue();
                    } else {
                        channel.sendMessageEmbeds(responses.allWalletsReply()).queue();
                    }
                    break;
                case XMR_PRICE_COMMAND:
                    channel.sendMessageEmbeds(responses.getPriceReply()).queue();
                    break;
                case XMR_TO_USD_COMMAND:
                    if (secondWordPresent) {
                        channel.sendMessageEmbeds(responses.xmrToUsdConversionReply(msgArray[1])).queue();
                    }
                    else {
                        channel.sendMessageEmbeds(responses.xmrToUsdConversionReply("")).queue();
                    }
                    break;
                case USD_TO_XMR_COMMAND:
                    if (secondWordPresent) {
                        channel.sendMessageEmbeds(responses.usdToXmrConversionReply(msgArray[1])).queue();
                    }
                    else {
                        channel.sendMessageEmbeds(responses.usdToXmrConversionReply("")).queue();
                    }
                    break;
                case XMR_TO_BTC_COMMAND:
                    if (secondWordPresent) {
                        channel.sendMessageEmbeds(responses.xmrToBtcConversionReply(msgArray[1])).queue();
                    }
                    else {
                        channel.sendMessageEmbeds(responses.xmrToBtcConversionReply("")).queue();
                    }
                    break;
                case BTC_TO_XMR_COMMAND:
                    if (secondWordPresent) {
                        channel.sendMessageEmbeds(responses.btcToXmrConversionReply(msgArray[1])).queue();
                    }
                    else {
                        channel.sendMessageEmbeds(responses.btcToXmrConversionReply("")).queue();
                    }
                    break;
                case DONATE_GENERAL_FUND_COMMAND:
                    channel.sendMessageEmbeds(responses.donateGeneralReply()).queue();
                    break;
                case DONATE_ART_COMMAND:
                    channel.sendMessageEmbeds(responses.donateArtReply()).queue();
                    break;
                case DONATE_DEV_COMMAND:
                    channel.sendMessageEmbeds(responses.donateDevReply()).queue();
                    break;
                default:
                    channel.sendMessage(responses.unknownCommandReply()).queue();
                    break;
            }

        }
        else {
            boolean hi = (msgArray[0].equals("hi") && msgArray.length == 1)
                    || (msgArray[0].equals("hey") && msgArray.length == 1) || (msgArray[0].equals("hello") && msgArray.length == 1)
                    || checkMessageForStringCombo(msgArray, new String[]{"Hi", MONEROCHAN_BOT_NAME}, msgReceivedEvent.getAuthor().getName())
                    || checkMessageForStringCombo(msgArray, new String[]{"Hey", MONEROCHAN_BOT_NAME}, msgReceivedEvent.getAuthor().getName())
                    || checkMessageForStringCombo(msgArray, new String[]{"Hello", MONEROCHAN_BOT_NAME}, msgReceivedEvent.getAuthor().getName());
            boolean uwu = checkMessageForStringCombo(msgArray, new String[]{"uwu"}, msgReceivedEvent.getAuthor().getName());
            boolean owo = checkMessageForStringCombo(msgArray, new String[]{"owo"}, msgReceivedEvent.getAuthor().getName());
            boolean heart = (msgArray[0].equals("<3") && msgArray.length == 1 && !authorName.equals(MONEROCHAN_BOT_NAME)) ||
                    checkMessageForStringCombo(msgArray, new String[]{MONEROCHAN_BOT_NAME, "<3"}, msgReceivedEvent.getAuthor().getName()) ||
                    checkMessageForStringCombo(msgArray, new String[]{"I", "love", MONEROCHAN_BOT_NAME}, msgReceivedEvent.getAuthor().getName());

            if(hi || uwu || owo || heart) {
                msgReceivedEvent.getChannel().sendTyping().queue();
            }

            if (hi) {
                channel.sendMessage(responses.hiReply(authorName)).queue();
            }
            else if (uwu) {
                channel.sendMessage(responses.uwuReply()).queue();
            }
            else if (owo) {
                channel.sendMessage(responses.owoReply()).queue();
            }
            else if (heart) {
                channel.sendMessage(responses.heartReply()).queue();
            }
        }
    }

    private static MessageChannel getMessageChannel(MessageReceivedEvent msgReceivedEvent) {
        MessageChannel channel;

        if (checkIfPrivateMsg(msgReceivedEvent)) {
            channel = msgReceivedEvent.getPrivateChannel();
        }
        else {
            channel = msgReceivedEvent.getChannel();
        }
        return channel;
    }

    private static boolean checkIfPrivateMsg(MessageReceivedEvent msgReceivedEvent) {
        try {
            msgReceivedEvent.getPrivateChannel();
            return true;
        }
        catch (IllegalStateException notPrivateChannel) {
            return false;
        }
    }

    private static boolean checkMessageForStringCombo(String[] messageArray, String[] searchWords, String authorName) {
        boolean allWordsFound = false;

        for (String searchWord : searchWords) {
            if ((Arrays.stream(messageArray).anyMatch(searchWord::equalsIgnoreCase)) &&
                    !authorName.equalsIgnoreCase(MONEROCHAN_BOT_NAME)) {
                allWordsFound = true;
            } else {
                return false;
            }
        }
        return allWordsFound;
    }

}
