package MonerochanBot;

import java.util.Random;

public class Artworks {

    private static final int TOTAL_MONEROCHAN_IMAGES = 31;

    public static final int ARTWORK_URL = 0;
    public static final int ARTIST_NAME = 1;
    public static final int ARTIST_URL = 2;

    private static final  String[] artworkUrls = {
            "https://www.monerochan.art/commissions/monerochan-beach.jpg",
            "https://www.monerochan.art/commissions/maidnero-chan.png",
            "https://www.monerochan.art/thumbnails/cheerleader_thumbnail.jpg",
            "https://www.monerochan.art/commissions/monerochan-wildwest.jpg",
            "https://www.monerochan.art/commissions/mememe.gif",
            "https://www.monerochan.art/commissions/mememe_bikini.gif",
            "https://www.monerochan.art/thumbnails/holdenmonaro_thumbnail.jpg",
            "https://www.monerochan.art/commissions/holdenmonaro.jpg",
            "https://www.monerochan.art/thumbnails/dandelion_thumbnail.jpg",
            "https://www.monerochan.art/commissions/volleyball_1.jpg",
            "https://www.monerochan.art/commissions/volleyball_2.jpg",
            "https://www.monerochan.art/thumbnails/wildwest2_thumbnail.jpg",
            "https://www.monerochan.art/thumbnails/irs_spanking_thumbnail.jpg",
            "https://gitlab.com/Winston69/discord-monerochan-bot/-/blob/master/img/lofi_monerochan.jpg",
            "https://www.monerochan.art/commissions/virgin_killer_sfw.png",
            "https://www.monerochan.art/thumbnails/date_thumbnail.jpg",
            "https://www.monerochan.art/thumbnails/wownerochan_headpat_thumbnail.jpg",
            "https://www.monerochan.art/commissions/mining_nonip.jpg",
            "https://www.monerochan.art/commissions/ribbons.jpg",
            "https://www.monerochan.art/thumbnails/fgc9_thumbnail.jpg",
            "https://www.monerochan.art/thumbnails/vtubing.png",
            "https://www.monerochan.art/commissions/boat.jpg",
            "https://www.monerochan.art/thumbnails/wownerochan_thumbnail.jpg",
            "https://www.monerochan.art/commissions/iwantyou_2.png",
            "https://www.monerochan.art/thumbnails/ribbons2_thumbnail.jpg",
            "https://www.monerochan.art/thumbnails/hammock_thumbnail.jpg",
            "https://www.monerochan.art/thumbnails/wownero_leash_thumbnail.png",
            "https://www.monerochan.art/commissions/assaultrifle.png",
            "https://www.monerochan.art/thumbnails/bladerunner_thumbnail.jpg",
            "https://xmrmemes.com/uploads/memes/f40ac5df1828c835b4c742bb968cb493.jpg",
            "https://i.redd.it/txvrhfjgsay61.png"
    };

   private static final  String[] artistsUrls = {
           "https://sketchmob.com/user-profile/rifqiaji",
            "https://skeb.jp/@umeckiti2",
            "https://sketchmob.com/user-profile/SenbonTsuki/",
            "https://sketchmob.com/user-profile/GIE/",
            "https://twitter.com/c_starlett",
            "https://twitter.com/c_starlett",
            "https://sketchmob.com/user-profile/Shirayuuki/",
            "https://sketchmob.com/user-profile/Shirayuuki/",
            "https://skeb.jp/@mi_me_i",
            "https://sketchmob.com/user-profile/GIE/",
            "https://sketchmob.com/user-profile/GIE/",
            "https://sketchmob.com/user-profile/pencamp/",
            "https://sketchmob.com/user-profile/Shylvania/",
            "https://www.fiverr.com/nanodsgn",
            "https://sketchmob.com/user-profile/Shylvania",
            "https://skeb.jp/@kageiratwintail",
            "https://twitter.com/xmr_draw",
            "https://sketchmob.com/user-profile/SenbonTsuki/",
            "https://skeb.jp/@umeckiti2",
            "https://sketchmob.com/user-profile/GIE/",
            "https://twitter.com/SashTheDee",
            "https://sketchmob.com/user-profile/SairamGtz/",
            "https://twitter.com/xmr_draw",
            "https://www.fiverr.com/phuthieu",
            "https://skeb.jp/@rara_toybox",
            "https://skeb.jp/@yamaP_mako",
            "https://sketchmob.com/user-profile/Shylvania/",
            "https://sketchmob.com/user-profile/rampage/",
            "https://skeb.jp/@tmc_tmc8",
            "https://xmrmemes.com/user/118",
            "https://www.reddit.com/user/Unkn8wn69/"
   };

   private static final String[] artists =  {
           "rifqiaji", "umeckiti2", "SenbonTsuki", "GIE", "c_starlett",
           "c_starlett", "Shirayuuki", "Shirayuuki", "mi_me_i", "GIE", "GIE", "pencamp", "Shylvania", "nanodsgn",
           "Shylvania", "kageiratwintail", "xmr_draw", "SenbonTsuki", "umeckiti2", "GIE", "SashTheDee", "SairamGtz",
           "xmr_draw", "phuthieu", "rara_toybox", "yamaP_mako", "Shylvania", "Rampage", "tmc_tmc8", "Jin", "Unkn8wn69's GF"
   };

    public String[] getMonerochanArt() {
        Random random = new Random();
        int randomInt = random.ints(0, TOTAL_MONEROCHAN_IMAGES).findFirst().getAsInt();
        return new String[] {artworkUrls[randomInt], artists[randomInt], artistsUrls[randomInt]};
    }

    public String[] getMonerochanMining() {
        return new String[] {artworkUrls[17], artists[17], artistsUrls[17]};
    }
}
