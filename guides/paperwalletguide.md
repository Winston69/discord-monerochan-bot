![Image](https://coindoo.com/wp-content/uploads/2018/11/monero-paper.jpg)
# Creating an XMR paper wallet using Tails

Prerequisites: A working PC, a USB stick (8GB or more), and another removable storage device.

Tails OS is a privacy-focused, Debian-based Linux distribution that runs completely on a computer's RAM. RAM is a volatile type of memory i.e. 
once the computer is turned off, the memory is gone and thus any information that was on it.

## Step 1: Download the Paper Wallet Generator

1. Download the [Paper Wallet Generator](https://github.com/moneromooo-monero/monero-wallet-generator/raw/master/monero-wallet-generator.html).
3. Extract the zip file(s).
4. Copy them to your removable storage device (not the USB your using for Tails).

#### Optional: Encrypt your Private Mnemonic Phrase
1. If you would like to encrypt your private mnemonic phrase with a password, you can download this offline [tool](https://github.com/luigi1111/xmr.llcoins.net/raw/master/site.zip).
3. Extract the zip file(s).
4. Copy them to your removable storage device (not the USB your using for Tails), like you just did for the Paper Wallet Generator.


## Step 2: Download Tails

- [Download Tails from the Tails website](https://tails.boum.org/install/download/index.en.html)

####  Verifying your Tails Download (optional, but highly recommended)

Windows:</br> 
1. Open up a command prompt (CMD).</br>
2. Type this command into the terminal: `certutil -hashfile filename SHA256`</br>
3. Replace filename with the exact name of the Tails download including the `.img` extension.
4. Press the 'Enter' key and it should spit out a SHA256 sum.
5. See if the checksum in the terminal matches the [first sha256 checksum on the Tails website](https://tails.boum.org/install/v2/Tails/amd64/stable/latest.json). If it does, continue to the next step. If it doesn't, your download is most likely corrupted although it is possible that an attacker performed a Man-in-the-Middle attack.

Linux:</br>
1. Open a terminal.</br>
2. Type this command into the terminal: `sha256sum filename`</br>
3. Replace filename with the exact name of the Tails download including the `.img` extension.
4. Press the 'Enter' key, and it should spit out a SHA256 sum.
5. See if the checksum in the terminal matches the [first sha256 checksum on the Tails website](https://tails.boum.org/install/v2/Tails/amd64/stable/latest.json). If it does, continue to the next step. If it doesn't, your download is most likely corrupted although it is possible that an attacker performed a Man-in-the-Middle attack.

Mac:</br>
1. Open a terminal.</br>
2. Type this command into the terminal: `shasum -a 256 filename`</br>
3. Replace filename with the exact name of the Tails download including the `.img` extension.
4. Press the 'Enter' key, and it should spit out a SHA256 sum.
5. See if the checksum in the terminal matches the [first sha256 checksum on the Tails website](https://tails.boum.org/install/v2/Tails/amd64/stable/latest.json). If it does, continue to the next step. If it doesn't, your download is most likely corrupted although it is possible that an attacker performed a Man-in-the-Middle attack.

## Step 3: Flash USB Stick with Tails

**Note:** flashing a USB will overwrite all of its contents i.e. delete everything.

Windows:
1. Download the [Rufus Tool](https://rufus.ie).
2. Open Rufus.
3. Click the 'select' option in Rufus and choose the tails image file you downloaded.
4. Click 'start'.
5. Once the flashing process is complete, **safely** eject the USB.

Linux & Mac:
1. Download the [Etcher Tool](https://www.balena.io/etcher/).
2. Open Etcher.
3. Select the 'Flash from File' option and choose the Tails image file you downloaded.
5. Click the 'Select Target' option and set your USB as the target device.
6. Select 'Flash!'.
7. Once the flashing process is complete, **safely** eject the USB.

Linux (Terminal):

Caution must be taken when using the `dd` command as it will begin overwriting the selected device as soon as you press 'Enter'. Please ensure that you are 100% certain that the `of` argument of the `dd` command is set to the device you intend to flash.

1. Open a terminal.
2. Type `lsblk` to see a list of block devices. 
3. Determine which one is your USB e.g. /dev/sdc
4. Unmount the USB using the command: `$ umount /dev/sd<?>`
2. Type the following command: `$ dd bs=4M if=/path/to/input.iso of=/dev/sd<?> conv=fdatasync`
3. Replace the `if` argument with the full path to your tails image file.
4. Replace the `of` argument with the device you want to flash e.g. /dev/sdc
5. Press the 'Enter' key to begin the flashing process.

## Step 4: Start Tails

1. Reboot your PC.
2. During its boot startup press the key to show the list of boot options. This key is different and is dependent on your motherboard. It is usually one of the function keys like 'F11'.
3. Select the Tails USB.
4. Once the Tails welcome screen has loaded, it will allow to select some settings. Click the plus button below 'Additional Settings', select 'Offline Mode' and then 'Disable all networking.'
5. Click 'start'.
6. Once the Tails desktop has loaded, insert the storage device with the Paper Wallet Generator on it into the computer. 

## Step 5: Create the Paper Wallet
1. Once your Tails desktop has loaded, insert the storage device containing the paper wallet generator.
2. Open the File Manager.
3. On the left side of the File Manager, scroll down to the bottom, it should have your USB there.
4. Click on it and it should have the `monero-wallet-generator.html` file.
5. Right-click on the file and choose 'copy'.
6. On the left side of the File Manager, find the 'Tor Browser' folder, open it, right-click inside the folder and choose 'paste'.
7. Right-click on the `monero-wallet-generator.html` file inside the 'Tor Browser' folder and select 'Open with Tor Browser.' You will get a pop-up saying:
> The Tor Browser is not ready. Start Anyway? </br> Yes / No
8. Select 'Yes'. 

The Offline Wallet Generator should now open in the Tor Browser. If you get an error about the browser being unable to open the file, ensure the file is located inside the 'Tor Browser' folder in the File Manager.

Note: Keep in mind that you are never actually connected to the internet or the Tor network, you are just using the browser to display the `html` file. The reason why the Tor Browser isn't ready is it cannot connect to the Tor network because we have disabled all networking. It can still display our `monero-wallet-generator.html` file because it is stored locally on the computer.

## Step 5: Creating the Paper Wallet

Your wallet will have four very important items:

```txt
Public Address: This is used to receive funds to the wallet and is safe to share.

Private Mnemonic Phrase: A human-readable format of your private spend key that can be used be used to recover your wallet. NEVER share your seed phrase.

Private Spend Key: The private spend key is used to send funds from the wallet. NEVER share your private spend key.

Private View Key: The private view key is to view transactions entering the wallet. This key is used to create view-only wallets that can see how much XMR a wallet has received although they will not being able to spend it.
```

You need either the **private** mnemonic phrase or the **private** spend key to be able to spend the funds of the wallet. Therefore, if you do not have access to either of those you have effectively lost all of XMR. It is **impossible** to recover a wallet without one of these. Do not share either of these with anyone, it is equivalent to you handing them your XMR wallet forever.

Make sure your paper wallet includes the public address, the mnemonic seed phrase (and/or private spend key), the private view key.

**Warning:** If you have chosen to encrypt your private mnemonic phrase, do not store the private spend key on your paper wallet as that will defeat the purpose of having your mnemonic phrase encrypted.

1. Copy the information as plain text and paste it into LibreOffice Writer. Alternatively, you can copy the information as QR codes too. 
2. Once you have the information in your LibreOffice document, print the document with the wallet information.
3. Clear your printer's memory as your wallet information may stay on there after you've completed everything. This is different on each printer so you will have to research how to do it. Alternatively, you can destroy your printer with a sledgehammer. 
4. Congratulations, you now have your paper XMR wallet, and you can sleep tight knowing you created it safely.
5. Store your paper wallet in a safe and secure location.

## Step 6: Backing up the Wallet
It is vital that you have multiple backups of your XMR wallet to avoid any boating accidents ;)

#### Backup Methods:

- Print multiple copies of your wallet.
- Create a KeePass database using the KeePassXC software that comes pre-installed with Tails and store the necessary information inside it.
- Create an encrypted file or device using the Veracrypt software that comes pre-installed with Tails and store the information on it.

Note: Use a password generator to create a strong pseudorandom password of **at least** 20 characters to encrypt the information. **Do not** attempt to create your own random password as humans are *terrible* at generating randomness.

## Congratulations
You have successfully created your paper XMR wallet, hopefully with many secure backups. Once you are 100% satisfied that you have made enough backups and have all of the necessary information to recover your XMR wallet, you can turn off your Tails PC and unplug the USB.

Reminder: Tails OS runs completely on a computer's RAM which is a volatile type of memory i.e. once the computer is turned off, the memory is gone and thus any information that was on it. Once you turn off your Tails PC, any information that was on it will be permanently destroyed.

## Sending XMR to your new wallet
To send XMR to the wallet, you must use the **public** address from the wallet. Please ensure that the address is correct before sending XMR to the wallet, even if you copy-pasted the address as there is malware that can detect XMR addresses in the clipboard and paste in a different one.

## Verifying the XMR was received
Because the Monero blockchain is private and untraceable, you won't be able to look up your Monero Public Address and confirm that the funds have arrived like you might with Bitcoin. This is good for privacy, but bad for convenience.

To securely verify the funds have arrived at your wallet, you will need to set up a View Only wallet. This is where that private view key comes in. You can easily create a view-only wallet in the Monero GUI wallet software.

1. Open the Monero GUI software.
2. Click the 'Restore wallet from keys or seed' option.
3. Select 'Restore from keys'.
4. In the 'Public address' box, put in your public address.
5. In the 'View Key' box, put in your private view key.
6. Leave the 'Spend Key' blank as you don't want the wallet to be able to spend any of your XMR.
7. Click 'Next' to access your view-only wallet.

Note: Your Monero GUI software may need to sync up to the blockchain before it can correctly display your XMR balance so don't panic if you don't see your XMR right away.
