# Pool Mining XMR using P2Pool

P2Pool is a decentralised, peer-to-peer Monero mining pool developed from scratch by SChernykh (also known as sech1). 

P2Pool combines the advantages of pool and solo mining; you still fully control your Monero node and what it mines, but you get frequent payouts like on a regular pool. Further, when you use P2Pool, you are increasing the strength of the Monero network.

## Getting Started
There are three things you need to be able to mine using P2Pool.

- Monero daemon (monerod)
- P2Pool
- XMRig

## Download the Monero daemon (monerod)

Note: If you have the Monero GUI or CLI installed, then you already have the daemon installed so you can move ahead to the next section.

1. Download the Monero GUI or CLI.
2. Locate the daemon in your file system. For Windows users, you can search for the daemon via the search icon located in the task bar (bottom left of your screen). For Linux users, by default, the monero programs should be located in `/usr/bin`.

## Download P2Pool

1. Go to the [Github for P2Pool](https://github.com/SChernykh/p2pool/releases) and download the release for your operating system i.e.

- Windows: p2pool-v1.5-windows-x64.zip
- Linux: p2pool-v1.5-linux-x64.tar.gz
- Mac: p2pool-v1.5-macos-x64.tar.gz

2. Extract the file.

## Download XMRig

1. Go to the [XMRig downloads section](https://xmrig.com/download).
2. Select the operating system relevant to you.
3. Download it.
4. Extract the file.

## Configuring the daemon

Note: If you aren't already synced to the Monero blockchain, the daemon will have to do this. This usually takes a few hours with an SSD. However, if you are using a mechanical HDD, this will take a **VERY LONG** time.

### Windows

1. Open a command prompt (CMD).
2. Change directory to the one with monerod.
2. To do this, type the following into the cmd: `cd /path/to/folder`
3. Replace the "/path/to/folder" with the folder that contains monerod.
4. Use the `dir` command to ensure the folder you are now in contains monerod.
5. Copy the following command into the terminal and press 'Enter'.

```
monerod.exe --zmq-pub tcp://127.0.0.1:18083 --disable-dns-checkpoints --enable-dns-blocklist
```

### Linux & Mac

1. Open a terminal.
2. Change directory to the one with monerod.
2. To do this, type the following into the cmd: `cd /path/to/folder`
3. Replace the "/path/to/folder" with the folder that contains monerod. This is usually `/usr/bin` for Linux users.
4. Use the `ls` command to ensure the folder you are now in contains monerod.
5. Copy the following command into the terminal and press 'Enter'.

```
./monerod --zmq-pub tcp://127.0.0.1:18083 --disable-dns-checkpoints --enable-dns-blocklist
```

## Configuring P2Pool

### Windows

1. Open a command prompt (CMD).
2. Change directory to the one with P2Pool.
2. To do this, type the following into the cmd: `cd /path/to/folder`
3. Replace the "/path/to/folder" with the folder that contains P2Pool.
4. Use the `dir` command to ensure the folder you are now in contains P2Pool.
5. Copy the following command into the terminal and press 'Enter'.

```
p2pool.exe --host 127.0.0.1 --wallet YOUR_MONERO_ADDRESS_HERE
```

### Linux & Mac

1. Open a terminal.
2. Change directory to the one with P2Pool.
2. To do this, type the following into the cmd: `cd /path/to/folder`
3. Replace the "/path/to/folder" with the folder that contains P2Pool. This is usually `/usr/bin` for Linux users.
4. Use the `ls` command to ensure the folder you are now in contains P2Pool.
5. Copy the following command into the terminal and press 'Enter'.

```
./p2pool --host 127.0.0.1 --wallet YOUR_MONERO_ADDRESS_HERE
```

## Configuring XMRig

### Windows

1. Open a command prompt (CMD).
2. Change directory to the one with XMRig.
2. To do this, type the following into the cmd: `cd /path/to/folder`
3. Replace the "/path/to/folder" with the folder that contains XMRig.
4. Use the `dir` command to ensure the folder you are now in contains XMRig.
5. Copy the following command into the terminal and press 'Enter'.

```
xmrig.exe -o 127.0.0.1:3333
```

### Linux & Mac

1. Open a terminal.
2. Change directory to the one with monerod.
2. To do this, type the following into the cmd: `cd /path/to/folder`
3. Replace the "/path/to/folder" with the folder that contains XMRig. This is usually `/usr/bin` for Linux users.
4. Use the `ls` command to ensure the folder you are now in contains XMRig.
5. Copy the following command into the terminal and press 'Enter'.

```
./xmrig -o 127.0.0.1:3333
```
